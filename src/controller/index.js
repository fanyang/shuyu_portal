import vue from 'vue';
import vue_router from 'vue-router';
import Vuex from 'vuex';
vue.use(vue_router);
vue.use(Vuex);
import index_view from '../views/index.vue';
import wallet from '../views/wallet.vue';
import scan_qr_code from '../views/scan_qr_code.vue';
import { check_lang_key } from '@/data/langs';
let url_lang = check_lang_key();
const router_link = [
    {
        path: '/',
        redirect: '/' + url_lang,
    },
    {
        path: '/' + url_lang,
        name: 'app',
        component: index_view
    },
    {
        path: '/' + url_lang + '/wallet/',
        name: 'wallet',
        component: wallet
    },
    {
        path: '/' + url_lang + '/scan_qr_code/',
        name: 'scan_qr_code',
        component: scan_qr_code
    }
];
const router_config = new vue_router({
    routes: router_link
});
const app = new vue({
    router: router_config,
    mounted() {
        document.dispatchEvent(new Event('render-event'));
    }
}).$mount('#app');
//# sourceMappingURL=index.js.map