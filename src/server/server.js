import { createApp } from '../controller/index.js'


export default context => {
  // 因为有可能会是异步路由钩子函数或组件，所以我们将返回一个 Promise，
    // 以便服务器能够等待所有的内容在渲染前，
    // 就已经准备就绪。
  return new Promise((resolve, reject) => {
    const { app, router } = createApp()

    // 设置服务器端 router 的位置
    router.push(context.url)

    // 等到 router 将可能的异步组件和钩子函数解析完
    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents()
      // 匹配不到的路由，执行 reject 函数，并返回 404
      if (!matchedComponents.length) {
        return reject({ code: 404 })
      }

      // Promise 应该 resolve 应用程序实例，以便它可以渲染
      resolve(app)
    }, reject)
  })
}

// const Vue = require('vue')
// import App from '../controller/app.vue'

// import app from '../controller/index.js'
// import index_view from '../views/index.vue'

// const app = new Vue({
//   // template: `<div>Hello World</div>`
//   render: h => h(index_view)
// })
// // 第 2 步：创建一个 renderer
// const template_config = {
//   template: require('fs').readFileSync('/Users/bandonli/work/shuyu_portal/public/index.html', 'utf-8')
// }
// const renderer = require('vue-server-renderer').createRenderer(template_config)

// // 第 3 步：将 Vue 实例渲染为 HTML
// renderer.renderToString(app, (err, html) => {
//   if (err) throw err
//   console.log(html)
//   // => <div data-server-rendered="true">Hello World</div>
// })

// 在 2.5.0+，如果没有传入回调函数，则会返回 Promise：
// renderer.renderToString(app).then(html => {
//   console.log(html)
// }).catch(err => {
//   console.error(err)
// })